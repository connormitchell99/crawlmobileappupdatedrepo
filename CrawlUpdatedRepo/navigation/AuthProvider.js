import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
import {db} from '../config';
import firestore from 'firebase';
export const AuthContext = createContext({});
export const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);
  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        login: async (email, password) => {
          try {
            await auth().signInWithEmailAndPassword(email, password);
          } catch (e) {
            console.log(e);
          }
        },
        register: async (email, password, displayName) => {
          try {
            await auth()
              .createUserWithEmailAndPassword(email, password, displayName)
              .then((res) => {
                firestore.collection(`/users/${res.user.uid}`).add({
                  firstName: res.additionalUserInfo.profile.given_name,
                  lastName: res.additionalUserInfo.profile.family_name,
                  email: email,
                  password: password,
                  displayName: displayName,
                  created_at: Date.now(),
                });
              });
          } catch (e) {
            console.log(e);
          }
        },
        logout: async () => {
          try {
            await auth().signOut();
          } catch (e) {
            console.error(e);
          }
        },
      }}>
      {children}
    </AuthContext.Provider>
  );
};
