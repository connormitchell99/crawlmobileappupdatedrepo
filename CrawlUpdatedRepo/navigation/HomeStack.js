import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home';
import EventTemplate from '../screens/EventTemplate';
import BarTemplate from '../screens/BarTemplate';
import NeighborhoodSearch from '../screens/NeighborhoodSearch';
import NeighborhoodSearchResults from '../screens/NeighborhoodSearchResults';
import BarSearch from '../screens/BarSearch';
import BarEvents from '../screens/BarEvents';
import OpenMaps from '../screens/OpenMaps';
import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
const Stack = createStackNavigator();
export default function HomeStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="EventTemplate" component={EventTemplate} />
      <Stack.Screen name="BarTemplate" component={BarTemplate} />
      <Stack.Screen name="BarEvents" component={BarEvents} />
      <Stack.Screen name="OpenMaps" component={OpenMaps} />
      <Stack.Screen name="NeighborhoodSearch" component={NeighborhoodSearch} />
      <Stack.Screen
        name="NeighborhoodSearchResults"
        component={NeighborhoodSearchResults}
      />
      <Stack.Screen name="BarSearch" component={BarSearch} />
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="SignupScreen" component={SignupScreen} />
    </Stack.Navigator>
  );
}
