import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Button,
} from 'react-native';
import Modal from 'react-native-modal';

export default class BarEvents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eventTitle: '',
      eventDescription: '',
      dayOfTheWeek: '',
      isModalVisible: false,
      eventTonight: '',
      noDealTonight: false,
    };
  }
  render() {
    const toggleModal = () => {
      this.state.isModalVisible = !this.state.isModalVisible;
    };
    const events = this.props.route.params.events;
    this.state.eventTonight = this.props.route.params.todaysEvent;
    var dOTW = '';
    var curr = new Date();
    var today = curr.getDay();
    const barId = this.props.route.params.barId;
    const phone = this.props.route.params.phone;
    const barEvents = [];
    for (var i = 0; i < events.length; i++) {
      if (barId === events[i]['hosted-by']) {
        barEvents.push(events[i]);
        dOTW = events[i]['day-of-the-week'];
        if (dOTW === '5efe2571fe3ede0050c8cbe7') {
          dOTW = 0;
        }
        if (dOTW === '5efe255b80f69473011c516f') {
          dOTW = 1;
        }
        if (dOTW === '5efe255f1aac083edc037d67') {
          dOTW = 2;
        }
        if (dOTW === '5efe2564446e68c4ac73f251') {
          dOTW = 3;
        }
        if (dOTW === '5efe2568b6e6b7a2494a3158') {
          dOTW = 4;
        }
        if (dOTW === '5efe256b639ad21a16d45ca7') {
          dOTW = 5;
        }
        if (dOTW === '5efe256e46a425c76a2e0799') {
          dOTW = 6;
        }
        if (today === dOTW) {
          this.state.eventTonight = events[i];
        }
      }
    }
    if (this.state.eventTonight === []){
        this.state.noDealTonight = true;
    }
    if (barEvents.length === 0) {
      return (
        <View style={{backgroundColor: '#2b2d42'}}>
          <View>
            <TouchableOpacity
              style={styles.allBack}
              onPress={() =>
                this.props.navigation.navigate('BarTemplate', {
                  phone: phone,
                  event: false,
                  barId: barId,
                })
              }>
              <Image
                style={styles.iconBack}
                source={require('../assets/icons/icons8-back-100.png')}
              />
              <Text style={styles.backText}>Back</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.noEvents}>
            <Text style={styles.header}>Sorry, no deals here :(</Text>
          </View>
          <View style={styles.remainder} />
        </View>
      );
    }
    if (this.state.noDealTonight === true) {
      return (
        <View style={{backgroundColor: '#2b2d42'}}>
          <TouchableOpacity
            style={styles.allBack}
            onPress={() =>
              this.props.navigation.navigate('BarTemplate', {
                phone: phone,
                event: false,
                barId: barId,
              })
            }>
            <Image
              style={styles.iconBack}
              source={require('../assets/icons/icons8-back-100.png')}
            />
            <Text style={styles.backText}>Back</Text>
          </TouchableOpacity>
          <FlatList
            data={barEvents}
            horizontal={true}
            keyExtractor={({id}, index) => id}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  style={styles.cards}
                  onPress={() => {
                    this.state.eventTitle = item.name;
                    this.state.eventDescription = item.description;
                    this.state.dayOfTheWeek = item['day-of-the-week'];
                    toggleModal();
                    console.log(this.state.isModalVisible);
                  }}>
                  <Image
                    style={styles.cardImage}
                    source={require('../assets/ruby.png')}
                  />
                  <Text style={styles.header}>{item.name}</Text>
                </TouchableOpacity>
              );
            }}
          />
          <View style={styles.todaysEvent}>
            <Text style={styles.bigHeader}>No Deals Today!</Text>
          </View>
          <View style={styles.remainder} />
        </View>
      );
    } if (this.state.noDealTonight === false) {
      return (
        <View style={{backgroundColor: '#2b2d42'}}>
          <TouchableOpacity
            style={styles.allBack}
            onPress={() =>
              this.props.navigation.navigate('BarTemplate', {
                phone: phone,
                event: false,
                barId: barId,
              })
            }>
            <Image
              style={styles.iconBack}
              source={require('../assets/icons/icons8-back-100.png')}
            />
            <Text style={styles.backText}>Back</Text>
          </TouchableOpacity>
          <FlatList
            data={barEvents}
            horizontal={true}
            keyExtractor={({id}, index) => id}
            renderItem={({item}) => {
              if (item['day-of-the-week'] === '5efe2571fe3ede0050c8cbe7') {
                var dOTW = 'Sunday';
              }
              if (item['day-of-the-week'] === '5efe255b80f69473011c516f') {
                var dOTW = 'Monday';
              }
              if (item['day-of-the-week'] === '5efe255f1aac083edc037d67') {
                var dOTW = 'Tuesday';
              }
              if (item['day-of-the-week'] === '5efe2564446e68c4ac73f251') {
                var dOTW = 'Wednesday';
              }
              if (item['day-of-the-week'] === '5efe2568b6e6b7a2494a3158') {
                var dOTW = 'Thursday';
              }
              if (item['day-of-the-week'] === '5efe256b639ad21a16d45ca7') {
                var dOTW = 'Friday';
              }
              if (item['day-of-the-week'] === '5efe256e46a425c76a2e0799') {
                var dOTW = 'Saturday';
              }
              return (
                <TouchableOpacity
                  style={styles.cards}
                  onPress={() => {
                    this.state.eventTitle = item.name;
                    this.state.eventDescription = item.description;
                    this.state.dayOfTheWeek = item['day-of-the-week'];
                    toggleModal();
                    console.log(this.state.isModalVisible);
                  }}>
                  <Image
                    style={styles.cardImage}
                    source={require('../assets/ruby.png')}
                  />
                  <Text style={styles.header}>{dOTW}</Text>
                  <Text style={styles.smallHeader}>{item.name}</Text>
                </TouchableOpacity>
              );
            }}
          />
          <View style={styles.todaysEvent}>
            <Text style={styles.bigHeader}>Today's Deal</Text>
            <TouchableOpacity>
              <Image
                style={styles.todaysEventImage}
                source={require('../assets/ruby.png')}
              />
              <Text style={styles.header}>{this.state.eventTonight.name}</Text>
              <Text style={styles.eventDescription}>
                {this.state.eventTonight.description}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.remainder}/>
        </View>

      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    marginBottom: 40,
  },
  remainder: {
    height: 550,
    backgroundColor: '#2b2d42',
  },
  eventDescription: {
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular',
    marginTop: 10,
    color: '#fefeff',
  },
  todaysEvent: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  todaysEventImage: {
    width: 400,
    height: 150,
    borderRadius: 20,
  },
  header: {
    fontFamily: 'SofiaPro-Regular',
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    color: '#fefeff',
  },
  smallHeader: {
    fontFamily: 'SofiaPro-Regular',
    fontWeight: 'bold',
    fontSize: 10,
    textAlign: 'center',
    color: '#fefeff',
  },
  bigHeader: {
    fontFamily: 'SofiaPro-Regular',
    fontWeight: 'bold',
    fontSize: 36,
    textAlign: 'center',
    color: '#fefeff',
  },
  noEvents: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'SofiaPro-Regular',
    marginTop: '50%',
    color: '#fefeff',
  },
  listItem: {
    maxWidth: '50%',
    flex: 0.5,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 4,
  },
  cards: {
    width: 200,
    marginLeft: 20,
    justifyContent: 'center',
  },
  cardImage: {
    width: '100%',
    height: 80,
    borderRadius: 10,
  },
  cardHeader: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
    color: '#fefeff',
  },
  iconBack: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  allBack: {
    flexDirection: 'row',
    marginTop: 45,
    marginBottom: 50,
  },
  backText: {
    marginTop: 3,
    color: '#f79256',
    fontWeight: 'bold',
  },
});
