import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  ImageBackground,
  PureComponent,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import stringSimilarity from 'string-similarity';

export default class BarSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      BarList: [],
      EventList: [],
      updatedBars: [],
      keyword: '',
      loading: true,
      routes: [
        {
          key: 'bars',
          title: 'Bars',
        },
        {
          key: 'events',
          title: 'Events',
        },
      ],
      index: 0,
      setIndex: 0,
      onIndexChange: 0,
    };
  }

  sortBySimilarity = (items) => {
    var dict = [];

    for (var i = 0; i < items.length; i++) {
      let item = items[i];
      var n = item.name;
      var d = item._id;
      var ph = item['phone-number-2'];
      var sim = this.getSimilarity(n, this.state.keyword);

      var obj = {name: n, phonenumber: ph, id: d, similarity: sim};
      if (dict.length == 0) {
        dict.push(obj);
      } else {
        var j = 0;
        for (; j < dict.length; j++) {
          if (sim > dict[j].similarity) {
            break;
          }
        }
        dict.splice(j, 0, obj);
      }

      //dict.push(obj);
      //console.log(obj);
    }
    return dict;
  };

  getSimilarity(name1, name2) {
    var name1Changed = name1
      .toLowerCase()
      .match(/[^_\W]+/g)
      .join(' ')
      .trim();
    var name2Changed = name2
      .toLowerCase()
      .match(/[^_\W]+/g)
      .join(' ')
      .trim();
    let x = stringSimilarity.compareTwoStrings(name1Changed, name2Changed);
    return x;
  }

  componentDidMount() {
    fetch(
      'https://api.webflow.com/collections/5eb9e422e9c3647d8b5eff63/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            BarList: result.items,
            updatedBars: this.sortBySimilarity(result.items),
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
    fetch(
      'https://api.webflow.com/collections/5efe26a316929855952dad60/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            EventList: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }

  render() {
    const {
      BarList,
      isLoaded,
      routes,
      index,
      EventList,
      updatedBars,
    } = this.state;

    this.state.keyword = this.props.route.params.keyword;

    const initialLayout = {
      width: Dimensions.get('window').width,
    };

    return (
      <View>
        <FlatList
          data={updatedBars}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => {
            console.log(updatedBars);
            return (
              <TouchableOpacity
                style={styles.result}
                onPress={() => {
                  this.props.navigation.navigate('BarTemplate', {
                    barId: item.id,
                    phone: item.phonenumber,
                    home: false,
                  });
                }}>
                <Text>{item.name}</Text>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  result: {
    height: 200,
    width: '100%',
    backgroundColor: '#fefeff',
    borderWidth: 1,
    borderColor: '#923a49',
  },
  rating: {
    height: 20,
    width: 30,
    backgroundColor: '#55ff00',
    borderRadius: 5,
    justifyContent: 'center',
    alignContent: 'center',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  tabs: {
    backgroundColor: '#2b2d42',
  },
  tabBar: {
    flexDirection: 'row',
    marginTop: 20,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
    color: '#fefeff',
  },
});
