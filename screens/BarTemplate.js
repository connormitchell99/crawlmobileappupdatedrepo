import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Button,
} from 'react-native';
import Lightbox from 'react-native-lightbox';
import Modal from 'react-native-modal';
import HeaderImageScrollView, {
  TriggeringView,
} from 'react-native-image-header-scroll-view';
import Mapview, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

Geolocation.getCurrentPosition();

export default class BarTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Bar: '',
      isLoaded: false,
      barName: '',
      barRating: '',
      openNow: '',
      reviews: [],
      barPhoneNumber: '',
      barHours: [],
      priceLevel: '',
      id: '0',
      photos: [],
      photoSource: '',
      photoWidth: '',
      photoHeight: '',
      photoReference: '',
      isModalVisible: false,
      currentLongitude: '',
      currentLatitude: '',
      eventTonight: [],
      events: [],
      barLong: '',
      barLat: '',
      barDescription: '',
      reviewRating: '',
      barId: '',
      phoneNumber: '',
      goToEvent: false,
      amenities: [],
      barAmenities: [],
      barNeighborhood: '',
      neighborhoods: [],
      openMaps: false,
      barLatS: '',
      barLongS: '',
    };
  }
  componentDidMount() {
    this.state.phoneNumber = this.props.route.params.phone;
    const barId = this.props.route.params.barId;
    fetch(
      `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=%2B1${this.state.phoneNumber}&inputtype=phonenumber&fields=place_id&key=AIzaSyBtK3qizx_CJb8YCfi_UEB4MRb3vBAF2D4`,
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            id: result.candidates,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
    fetch(
      'https://api.webflow.com/collections/5efe26a316929855952dad60/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          events: result.items,
          isLoading: true,
        });
      });
    fetch(
      `https://api.webflow.com/collections/5eb9e422e9c3647d8b5eff63/items/${barId}?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee`,
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            barDescription: result.items[0]['description-3'],
            barAmenities: result.items[0].amenities,
            barNeighborhood: result.items[0].neighborhood,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
    fetch(
      'https://api.webflow.com/collections/5efa3c7d21cb20f19d17c254/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            amenities: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
    fetch(
      'https://api.webflow.com/collections/5efb842ecdaac9fc67995620/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            neighborhoods: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }
  render() {
    this.state.barId = this.props.route.params.barId;
    const eventDescription = this.props.route.params.description;
    this.state.isModalVisible = this.props.route.params.visible;
    this.state.goToEvent = this.props.route.params.event;
    const goHome = this.props.route.params.home;
    const todaysEvent = this.props.route.params.todaysEvent;
    var curr = new Date();
    var today = curr.getDay();
    var dOTW = '';
    for (var i = 0; i < this.state.events.length; i++) {
      dOTW = this.state.events[i]['day-of-the-week'];
      if (dOTW === '5efe2571fe3ede0050c8cbe7') {
        dOTW = 0;
      }
      if (dOTW === '5efe255b80f69473011c516f') {
        dOTW = 1;
      }
      if (dOTW === '5efe255f1aac083edc037d67') {
        dOTW = 2;
      }
      if (dOTW === '5efe2564446e68c4ac73f251') {
        dOTW = 3;
      }
      if (dOTW === '5efe2568b6e6b7a2494a3158') {
        dOTW = 4;
      }
      if (dOTW === '5efe256b639ad21a16d45ca7') {
        dOTW = 5;
      }
      if (dOTW === '5efe256e46a425c76a2e0799') {
        dOTW = 6;
      }
      if (today === dOTW && this.state.events[i]['hosted-by'] === barId) {
        this.state.eventTonight = this.state.events[i];
      }
    }
    if (this.state.goToEvent === true) {
      this.props.navigation.navigate('BarEvents', {
        events: this.state.events,
        barId: this.state.barId,
        phone: this.state.phoneNumber,
        todaysEvent: todaysEvent,
      });
      console.log(this.state.eventTonight);
    }
    fetch(
      `https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyBtK3qizx_CJb8YCfi_UEB4MRb3vBAF2D4&place_id=${this.state.id[0].place_id}&fields=price_level,name,rating,formatted_phone_number,review,photo,opening_hours,geometry`,
    )
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          isLoaded: true,
          barName: result.result.name,
          openNow: result.result.opening_hours.open_now,
          barHours: result.result.opening_hours.weekday_text,
          barPhoneNumber: result.result.formatted_phone_number,
          reviews: result.result.reviews,
          priceLevel: result.result.price_level,
          barRating: result.result.rating,
          photos: result.result.photos,
          photoWidth: result.result.photos[0].width,
          photoHeight: result.result.photos[0].height,
          photoReference: result.result.photos[0].photo_reference,
          barLong: result.result.geometry.location.lng,
          barLat: result.result.geometry.location.lat,
        });
        this.state.barLatS = this.state.barLat;
        this.state.barLongS = this.state.barLong;
        var lat = Number(this.state.barLat);
        var long = Number(this.state.barLong);
        this.state.barLat = lat;
        this.state.barLong = long;
      });
    const {
      barName,
      openNow,
      barHours,
      barPhoneNumber,
      reviews,
      priceLevel,
      barRating,
      photos,
      photoSource,
      photoWidth,
      photoHeight,
      photoReference,
      barLong,
      barLat,
      barId,
      reviewRating,
      amenities,
      barAmenities,
      barNeighborhood,
      neighborhoods,
    } = this.state;
    var curr = new Date();
    var today = curr.getDay();
    var dotw = '';
    var openOrClosed = '';
    var oCStyle = '';
    var ratingStyle = '';
    var dollars = '';
    var remainder = '';
    if (today === 0) {
      dotw = barHours[6];
    }
    if (today === 1) {
      dotw = barHours[0];
    }
    if (today === 2) {
      dotw = barHours[1];
    }
    if (today === 3) {
      dotw = barHours[2];
    }
    if (today === 4) {
      dotw = barHours[3];
    }
    if (today === 5) {
      dotw = barHours[4];
    }
    if (today === 6) {
      dotw = barHours[5];
    }
    if (openNow === true) {
      openOrClosed = 'Open';
      oCStyle = styles.open;
    }
    if (openNow === false) {
      openOrClosed = 'Closed';
      oCStyle = styles.closed;
    }
    if (barRating > 0 && barRating < 2) {
      ratingStyle = styles.badRating;
    }
    if (barRating > 1.9 && barRating < 3) {
      ratingStyle = styles.mediumBadRating;
    }
    if (barRating > 2.9 && barRating < 4) {
      ratingStyle = styles.okayRating;
    }
    if (barRating > 3.9 && barRating < 5) {
      ratingStyle = styles.goodRating;
    }
    if (priceLevel === 1) {
      dollars = '$';
      remainder = '$$';
    }
    if (priceLevel === 2) {
      dollars = '$$';
      remainder = '$';
    }
    if (priceLevel === 3) {
      dollars = '$$$';
      remainder = '$$';
    }
    if (priceLevel === 4) {
      dollars = '$$$$';
      remainder = '$';
    }
    if (priceLevel === 5) {
      dollars = '$$$$$';
      remainder = '';
    }
    for (var j = 0; j < neighborhoods.length; j++) {
      if (barNeighborhood === neighborhoods[j]._id) {
        var ne = neighborhoods[j].name;
      }
    }
    return (
      <ScrollView style={styles.font}>
        <View>
          <HeaderImageScrollView
            maxHeight={300}
            minHeight={0}
            headerImage={{
              uri: `https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyBtK3qizx_CJb8YCfi_UEB4MRb3vBAF2D4&maxwidth=${photoWidth}&maxheight=${photoHeight}&photoreference=${photoReference}`,
            }}
            renderForeground={() => (
              <TouchableOpacity
                style={styles.allBack}
                onPress={() => {
                  if (goHome === false) {
                    this.props.navigation.navigate('BarSearch');
                  } else {
                    this.props.navigation.navigate('Home');
                  }
                }}>
                <Image
                  style={styles.iconBack}
                  source={require('../assets/icons/icons8-back-100.png')}
                />
                <Text style={styles.backText}>Back</Text>
              </TouchableOpacity>
            )}>
            <TriggeringView onHide={() => console.log('text hidden')}>
              <View style={{marginLeft: 10}}>
                <View style={styles.container}>
                  <Text style={styles.title}>{barName}</Text>
                  <View style={styles.icons}>
                    <TouchableOpacity>
                      <Image
                        style={styles.icon}
                        source={require('../assets/icons/icons8-level-up-100.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.iconPin}
                    source={require('../assets/icons/icons8-map-pin-50.png')}
                  />
                  <Text style={styles.neighborhood}>{ne}</Text>
                </View>
                <View style={styles.rowContainer}>
                  <View style={ratingStyle}>
                    <Text style={styles.ratingText}>{barRating}</Text>
                  </View>
                  <Text style={styles.dollars}>{dollars}</Text>
                  <Text style={styles.remainder}>{remainder}</Text>
                </View>
                <View style={styles.rowContainer}>
                  <Text style={oCStyle}>{openOrClosed} </Text>
                  <Text style={styles.font}>{dotw}</Text>
                </View>
                <TouchableOpacity style={styles.safety}>
                  <Image
                    source={require('../assets/icons/icons8-protection-mask-50.png')}
                    style={styles.mask}
                  />
                  <Text style={styles.font}>Health and Safety Measures</Text>
                  <Image
                    source={require('../assets/icons/icons8-more-than-50.png')}
                    style={styles.forwardArrow}
                  />
                </TouchableOpacity>
                <View style={styles.rowContainer}>
                  <Text style={styles.font}>{this.state.barDescription}</Text>
                </View>
                <TouchableOpacity
                  style={styles.safety}
                  onPress={() => {
                    this.props.navigation.navigate('BarEvents', {
                      events: this.state.events,
                      barId: this.state.barId,
                      phone: this.state.phoneNumber,
                      todaysEvent: this.state.eventTonight,
                    });
                  }}>
                  <Image
                    source={require('../assets/icons/icons8-beer-50.png')}
                    style={styles.mask}
                  />
                  <Text style={styles.font}>Deals and Events</Text>
                  <Image
                    source={require('../assets/icons/icons8-more-than-50.png')}
                    style={styles.forwardArrow2}
                  />
                </TouchableOpacity>
              </View>
              <FlatList
                data={barAmenities}
                keyExtractor={({id}, index) => id}
                horizontal={true}
                renderItem={({item}) => {
                  for (var j = 0; j < amenities.length; j++) {
                    if (item === amenities[j]._id) {
                      var am = amenities[j].name;
                    }
                  }
                  return <Text style={styles.amenities}>{am}</Text>;
                }}
              />
              <View>
                <FlatList
                  data={photos}
                  keyExtractor={({id}, index) => id}
                  horizontal={true}
                  renderItem={({item}) => {
                    return (
                      <Lightbox
                        navigator={this.props.navigator}
                        style={styles.photoList}>
                        <Image
                          style={styles.fullScreenPhoto}
                          source={{
                            uri: `https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyBtK3qizx_CJb8YCfi_UEB4MRb3vBAF2D4&maxwidth=${item.width}&maxheight=${item.height}&photoreference=${item.photo_reference}`,
                          }}
                        />
                      </Lightbox>
                    );
                  }}
                />
                <View style={styles.mapContainer}>
                  <Mapview
                    style={styles.map}
                    region={{
                      latitude: barLat,
                      longitude: barLong,
                      latitudeDelta: 0.002,
                      longitudeDelta: 0.002,
                    }}>
                    <Marker
                      coordinate={{latitude: barLat, longitude: barLong}}
                      pinColor="#2b2d42"
                      title={barName}
                    />
                  </Mapview>
                </View>
                <TouchableOpacity
                  style={styles.directionContainer}
                  onPress={() => {
                    this.props.navigation.navigate('OpenMaps', {
                      barLat: this.state.barLatS,
                      barLong: this.state.barLongS,
                      barName: this.state.barName,
                    });
                  }}>
                  <Text style={styles.directions}>Get Directions</Text>
                </TouchableOpacity>
                <Text style={styles.title}>Reviews</Text>
                <FlatList
                  data={reviews}
                  keyExtractor={({id}, index) => id}
                  renderItem={({item}) => {
                    var reviewRatingStyle = '';
                    if (item.rating === 1) {
                      reviewRatingStyle = styles.badRating;
                    }
                    if (item.rating === 2) {
                      reviewRatingStyle = styles.mediumBadRating;
                    }
                    if (item.rating === 3) {
                      reviewRatingStyle = styles.okayRating;
                    }
                    if (item.rating === 4) {
                      reviewRatingStyle = styles.goodRating;
                    }
                    if (item.rating === 5) {
                      reviewRatingStyle = styles.goodRating;
                    }
                    return (
                      <View style={styles.reviewContainer}>
                        <Image
                          style={styles.profilePic}
                          source={{uri: item.profile_photo_url}}
                        />
                        <View style={styles.reviewRatings}>
                          <View style={reviewRatingStyle}>
                            <Text style={styles.ratingText}>{item.rating}</Text>
                          </View>
                          <Text style={styles.reviewText}>{item.text}</Text>
                          <Text style={styles.reviewAuthor}>
                            - {item.author_name}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            </TriggeringView>
          </HeaderImageScrollView>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  amenities: {
    marginLeft: 10,
    marginTop: 10,
    fontFamily: 'SofiaPro-Regular',
    fontWeight: 'bold',
  },
  neighborhood: {
    marginLeft: 10,
    fontFamily: 'SofiaPro-Regular',
    fontWeight: 'bold',
  },
  reviewAuthor: {
    marginTop: 15,
  },
  reviewRatings: {
    flexDirection: 'column',
    marginTop: 5,
  },
  mapContainer: {
    marginTop: 20,
  },
  events: {
    flexDirection: 'row',
  },
  noDeals: {
    height: 200,
    width: 200,
    backgroundColor: 'grey',
  },
  cards: {
    width: 200,
    marginLeft: 15,
    marginTop: 15,
  },
  cardImage: {
    width: '100%',
    height: '37%',
    borderRadius: 10,
  },
  modal: {
    height: 400,
    width: 300,
    backgroundColor: '#fefeff',
    borderRadius: 20,
  },
  map: {
    flex: 1,
    height: 200,
  },
  directions: {
    fontFamily: 'SofiaPro-Regular',
    marginLeft: 10,
  },
  directionContainer: {
    justifyContent: 'center',
    height: 40,
    borderWidth: 1,
    borderColor: 'grey',
  },
  font: {
    fontFamily: 'SofiaPro-Regular',
  },
  icon: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  iconPin: {
    width: 25,
    height: 25,
    marginBottom: 10,
  },
  profilePic: {
    width: 25,
    height: 25,
    marginLeft: 20,
    marginRight: 20,
  },
  mask: {
    width: 25,
    height: 25,
    marginLeft: 10,
    marginRight: 15,
  },
  forwardArrow: {
    width: 25,
    height: 25,
    marginLeft: 100,
  },
  forwardArrow2: {
    width: 25,
    height: 25,
    marginLeft: 172,
  },
  iconBack: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  allBack: {
    flexDirection: 'row',
    marginTop: 45,
  },
  backText: {
    marginTop: 3,
    color: '#f79256',
    fontWeight: 'bold',
  },
  icons: {
    marginLeft: 'auto',
    marginRight: 40,
    flexDirection: 'row',
  },
  container: {
    flexDirection: 'row',
    marginTop: 10,
  },
  photo: {
    width: '100%',
    height: 300,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'SofiaPro-Regular',
    marginBottom: 10,
    marginLeft: 5,
  },
  photoList: {
    width: 150,
    height: 150,
    marginLeft: 20,
    marginTop: 20,
  },
  fullScreenPhoto: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  open: {
    color: 'green',
    fontFamily: 'SofiaPro-Regular',
  },
  closed: {
    color: 'red',
    fontFamily: 'SofiaPro-Regular',
  },
  badRating: {
    backgroundColor: 'red',
    width: 30,
    height: 20,
    borderRadius: 5,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mediumBadRating: {
    backgroundColor: 'orange',
    width: 30,
    height: 20,
    borderRadius: 5,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  okayRating: {
    backgroundColor: 'orange',
    width: 30,
    borderRadius: 5,
    height: 20,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  goodRating: {
    backgroundColor: 'green',
    width: 30,
    height: 20,
    borderRadius: 5,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ratingText: {
    color: '#fefeff',
    fontSize: 16,
    fontFamily: 'SofiaPro-Regular',
  },
  dollars: {
    color: 'green',
    fontSize: 16,
    fontFamily: 'SofiaPro-Regular',
  },
  remainder: {
    color: 'grey',
    fontSize: 16,
    fontFamily: 'SofiaPro-Regular',
  },
  rowContainer: {
    flexDirection: 'row',
    marginTop: 5,
  },
  reviewContainer: {
    flexDirection: 'row',
    marginTop: 15,
    borderWidth: 1,
    borderColor: 'grey',
    marginRight: 10,
    marginLeft: 10,
  },
  reviewText: {
    marginRight: 80,
    marginLeft: 15,
    fontFamily: 'SofiaPro-Regular',
  },
  safety: {
    flexDirection: 'row',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'grey',
    height: 40,
    alignItems: 'center',
  },
});
