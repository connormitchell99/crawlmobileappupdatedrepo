import React, {useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  Button,
  Animated,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import Modal from 'react-native-modal';

var isHidden = true;
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bars: [],
      keywordValue: 'Keyword',
      neighborhoodValue: 'Neighborhood',
      events: [],
      isLoading: true,
      visible: false,
      bounceValue: new Animated.Value(100),
    };
  }
  componentDidMount() {
    fetch(
      'https://api.webflow.com/collections/5efe26a316929855952dad60/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          events: result.items,
          isLoading: true,
        });
      });
    fetch(
      'https://api.webflow.com/collections/5eb9e422e9c3647d8b5eff63/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          bars: result.items,
          isLoading: true,
        });
      });
  }
  _toggleSubview() {
    var toValue = 800;

    if (isHidden) {
      toValue = 0;
    }
    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(this.state.bounceValue, {
      toValue: toValue,
      velocity: 3,
      tension: 2,
      friction: 8,
      useNativeDriver: true,
    }).start();

    isHidden = !isHidden;
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.logo}
            source={require('../assets/navy_tag.png')}
          />
          <View style={styles.inputContainer}>
            <Image
              style={styles.iconLeft}
              source={require('../assets/icons/icons8-filter-50.png')}
            />
            <TextInput
              style={styles.input}
              onChangeText={(text) => this.setState({keywordValue: text})}
              value={this.state.keywordValue}
              onSubmitEditing={() => {
                this.props.navigation.navigate('BarSearch', {
                  keyword: this.state.keywordValue,
                });
                //this.props.navigation.navigate('BarSearch')
              }}
            />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('BarSearch', {
                  keyword: this.state.keywordValue,
                });
                //this.props.navigation.navigate('BarSearch')
              }}>
              <Image
                style={styles.iconRight}
                source={require('../assets/icons/icons8-search-50.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputContainer}>
            <TouchableOpacity
              onPress={() => {
                this._toggleSubview();
              }}>
              <Image
                style={styles.iconLeft}
                source={require('../assets/icons/icons8-map-pin-50.png')}
              />
            </TouchableOpacity>
            <TextInput
              style={styles.input}
              onChangeText={(text) => this.setState({neighborhoodValue: text})}
              value={this.state.neighborhoodValue}
              onSubmitEditing={() => {
                this.props.navigation.navigate('BarSearch', {
                  keyword: this.state.neighborhoodValue,
                });
                //this.props.navigation.navigate('BarSearch')
              }}
            />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('BarSearch', {
                  keyword: this.state.neighborhoodValue,
                });
                //this.props.navigation.navigate('BarSearch')
              }}>
              <Image
                style={styles.iconRight}
                source={require('../assets/icons/icons8-search-50.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.section}>
          <Text style={styles.headerText}>Today's Deals</Text>
          <Text style={styles.subheaderText}>What's poppin' today</Text>
          <View>
            <FlatList
              horizontal
              data={this.state.events}
              keyExtractor={({id}, index) => id}
              renderItem={({item}) => {
                var host;
                var curr = new Date();
                var today = curr.getDay();
                var dOTW = item['day-of-the-week'];
                for (var i = 0; i < this.state.bars.length; i++) {
                  if (this.state.bars[i]._id === item['hosted-by']) {
                    host = this.state.bars[i].name;
                    var phoneNumber = this.state.bars[i]['phone-number-2'];
                  }
                }
                if (dOTW === '5efe2571fe3ede0050c8cbe7') {
                  dOTW = 0;
                }
                if (dOTW === '5efe255b80f69473011c516f') {
                  dOTW = 1;
                }
                if (dOTW === '5efe255f1aac083edc037d67') {
                  dOTW = 2;
                }
                if (dOTW === '5efe2564446e68c4ac73f251') {
                  dOTW = 3;
                }
                if (dOTW === '5efe2568b6e6b7a2494a3158') {
                  dOTW = 4;
                }
                if (dOTW === '5efe256b639ad21a16d45ca7') {
                  dOTW = 5;
                }
                if (dOTW === '5efe256e46a425c76a2e0799') {
                  dOTW = 6;
                }
                if (item['hosted-by'] === '5efa3c60372db56b2ca6df4d') {
                  host = 'Gaslight Chicago';
                }
                if (today === dOTW) {
                  return (
                    <TouchableOpacity
                      style={styles.cards}
                      onPress={() => {
                        this.props.navigation.navigate('BarTemplate', {
                          phone: phoneNumber,
                          visible: true,
                          description: item.name,
                          event: true,
                          barId: item['hosted-by'],
                          home: true,
                          todaysEvent: item,
                        });
                        this.props.navigation.navigate('BarTemplate');
                      }}>
                      <Image
                        style={styles.cardImage}
                        source={require('../assets/ruby.png')}
                      />
                      <Text style={styles.cardHeader}>{host}</Text>
                      <Text style={styles.subCardText}>{item.name}</Text>
                    </TouchableOpacity>
                  );
                }
              }}
            />
          </View>
          <FormButton
            buttonTitle="Logout"
            onPress={() => {
              return (
                <Modal>
                  <Text>Test</Text>
                </Modal>
              );
            }}
          />
          <View style={styles.section} />
        </View>
        <Animated.View
          style={[
            styles.subView,
            {transform: [{translateY: this.state.bounceValue}]},
          ]}>
          <Text>This is a sub view</Text>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  subView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFFFFF',
    minHeight: 100,
    maxHeight: 600,
  },
  inputContainer: {
    backgroundColor: '#fefeff',
    flexDirection: 'row',
    width: '90%',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 10,
  },
  containerInput: {
    maxHeight: 60,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fefeff',
    borderRadius: 10,
  },
  iconLeft: {
    width: 25,
    height: 25,
    marginRight: 60,
  },
  iconRight: {
    width: 25,
    height: 25,
    marginLeft: 60,
  },
  text: {
    color: '#2b2d42',
    fontSize: 24,
    fontWeight: 'bold',
  },
  header: {
    backgroundColor: '#2b2d42',
    height: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: '30%',
    width: '60%',
    marginTop: 20,
    marginBottom: 80,
  },
  test: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    color: '#2b2d42',
    height: 40,
    borderColor: '#fefeff',
    borderWidth: 1,
    width: '40%',
    marginTop: 20,
    backgroundColor: '#fefeff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  headerText: {
    fontSize: 36,
    fontFamily: 'SofiaPro-Regular',
  },
  section: {
    marginLeft: 10,
    marginTop: 10,
  },
  cards: {
    width: 200,
    marginRight: 15,
  },
  cardImage: {
    width: '100%',
    height: 80,
    borderRadius: 10,
  },
  cardHeader: {
    fontWeight: 'bold',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 24,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
  },
  subheaderText: {
    marginBottom: 20,
    fontFamily: 'SofiaPro-Regular',
  },
  subCardText: {
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  neighborhoodButton: {
    backgroundColor: '#f79256',
    marginTop: 20,
    borderRadius: 20,
  },
  textOnBlue: {
    color: '#fefeff',
  },
});
