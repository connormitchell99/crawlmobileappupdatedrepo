import React from 'react';
import OpenMap from 'react-native-open-map';
import {Text, View} from 'react-native';

export default class OpenMaps extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barLat: '',
      barLong: '',
      barName: '',
    };
  }
  render() {
    this.state.barLat = this.props.route.params.barLat;
    this.state.barLong = this.props.route.params.barLong;
    this.state.barName = this.props.route.params.barName;
    OpenMap.show({
      latitude: this.state.barLat,
      longitude: this.state.barLong,
      title: this.state.barName,
      cancelText: 'Close',
      actionSheetTitle: 'Choose how to get there',
      actionSheetMessage: 'Available applications',
    });
    this.props.navigation.navigate('BarTemplate');
    return <View style={{backgroundColor: '#2b2d42'}} />;
  }
}
