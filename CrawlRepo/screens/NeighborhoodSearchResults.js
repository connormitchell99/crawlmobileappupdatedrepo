import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  ImageBackground,
  PureComponent,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';

export default class NeighborhoodSearchResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      BarList: [],
      EventList: [],
      updatedBars: [],
      loading: true,
      routes: [
        {
          key: 'bars',
          title: 'Bars',
        },
        {
          key: 'events',
          title: 'Events',
        },
      ],
      index: 0,
      setIndex: 0,
      onIndexChange: 0,
    };
  }
  _renderTabBar = (props) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = Animated.color(
            Animated.round(
              Animated.interpolate(props.position, {
                inputRange,
                outputRange: inputRange.map((inputIndex) =>
                  inputIndex === i ? 255 : 0,
                ),
              }),
            ),
            0,
            0,
          );

          return (
            <TouchableOpacity
              style={styles.tabItem}
              onPress={() => this.setState({index: i})}>
              <Animated.Text style={styles.tabItem}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  componentDidMount() {
    fetch(
      'https://api.webflow.com/collections/5eb9e422e9c3647d8b5eff63/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            BarList: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
    fetch(
      'https://api.webflow.com/collections/5efe26a316929855952dad60/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            EventList: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }

  render() {
    function onIndexChange(index) {
      if (index === 0) {
        index = 1;
      } else {
        index = 0;
      }
    }
    const {
      BarList,
      isLoaded,
      routes,
      index,
      EventList,
      updatedBars,
    } = this.state;
    const text = this.props.route.params.ids;
    var i = 0;
    const FirstRoute = () => (
      <View>
        <FlatList
          data={BarList}
          keyExtractor={({item}) => {
            BarList._id;
          }}
          renderItem={({item}) => {
            if (item.neighborhood === text) {
              updatedBars[i] = item._id;
              i++;
              return (
                <TouchableOpacity
                  style={styles.result}
                  onPress={() => {
                    this.props.navigation.navigate('BarTemplate', {
                      phone: item['phone-number-2'],
                      barId: item._id,
                      home: false,
                    });
                  }}>
                  <Text style={styles.header}>{item.name}</Text>
                  <View style={styles.rating}>
                    <Text>8.5</Text>
                  </View>
                </TouchableOpacity>
              );
            }
          }}
        />
      </View>
    );
    const SecondRoute = () => (
      <View>
        <FlatList
          data={EventList}
          keyExtractor={({item}) => {
            EventList._id;
          }}
          renderItem={({item}) => {
            for (var j = 0; j < updatedBars.length; j++) {
              if (updatedBars[j] === item['hosted-by']) {
                return (
                  <View style={styles.result}>
                    <Text style={styles.header}>{item.name}</Text>
                  </View>
                );
              }
            }
          }}
        />
      </View>
    );

    const initialLayout = {
      width: Dimensions.get('window').width,
    };

    const renderScene = SceneMap({
      bars: FirstRoute,
      events: SecondRoute,
    });
    return (
      <TabView
        style={styles.tabs}
        navigationState={{index, routes}}
        renderTabBar={this._renderTabBar}
        renderScene={renderScene}
        onIndexChange={(index) => {
          this.setState({index});
        }}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  result: {
    height: 200,
    width: '100%',
    backgroundColor: '#fefeff',
    borderWidth: 1,
    borderColor: '#923a49',
  },
  rating: {
    height: 20,
    width: 30,
    backgroundColor: '#55ff00',
    borderRadius: 5,
    justifyContent: 'center',
    alignContent: 'center',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  tabs: {
    backgroundColor: '#2b2d42',
  },
  tabBar: {
    flexDirection: 'row',
    marginTop: 20,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
    color: '#fefeff',
  },
});
