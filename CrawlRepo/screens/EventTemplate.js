import React, {useEffect, useState, Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
} from 'react-native';

export default class EventTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      EventList: [],
      loading: true,
      googleBar: [],
    };
  }
  componentDidMount() {
    const text = this.props.route.params.ids;
    fetch(
      `https://api.webflow.com/collections/5efe26a316929855952dad60/items/${text}?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee`,
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            EventList: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }
  render() {
    const {EventList} = this.state;
    return (
      <View>
        <FlatList
          data={EventList}
          keyExtractor={({ids}, index) => ids}
          renderItem={({item}) => {
            return (
              <View>
                <Image
                  style={styles.photo}
                  source={require('../assets/ruby.png')}
                />
                <View>
                  <Text style={styles.title}>{item.name}</Text>
                </View>
              </View>
            );
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  test: {
    justifyContent: 'center',
    alignContent: 'center',
    color: 'black',
  },
  photo: {
    width: '100%',
    maxHeight: 200,
  },
  title: {
    fontSize: 36,
    fontWeight: 'bold',
  },
});
