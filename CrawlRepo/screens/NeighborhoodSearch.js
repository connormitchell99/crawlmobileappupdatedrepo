import React, {useEffect, useState, PureComponent} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  TextInput,
  ImageBackground,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class NeighborhoodSearch extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      NeighborhoodList: [],
      loading: true,
    };
  }
  static navigationOptions = {
    title: 'List of Neighborhoods',
  };
  componentDidMount() {
    fetch(
      'https://api.webflow.com/collections/5efb842ecdaac9fc67995620/items?api_version=1.0.0&access_token=8fedd76db2a787e9f37288d95af81a59846b3b8de59c7a48dfcd31ada58240ee',
    )
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            NeighborhoodList: result.items,
          });
        },
        (error) => {
          console.log('Error fetching data-----------', error);
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }
  render() {
    const {NeighborhoodList, isLoaded} = this.state;
    if (isLoaded) {
      return (
        <View style={styles.body}>
          <FlatList
            style={styles.container}
            data={NeighborhoodList}
            keyExtractor={({item}) => {
              NeighborhoodList._id;
            }}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  style={styles.container}
                  onPress={() => {
                    this.props.navigation.navigate(
                      'NeighborhoodSearchResults',
                      {
                        ids: item._id,
                      },
                    );
                    this.props.navigation.navigate('NeighborhoodSearchResults');
                  }}>
                  <ImageBackground
                    source={{uri: `${item.photo.url}`}}
                    style={styles.images}>
                    <Text>{item.name}</Text>
                    <Text>{item._id}</Text>
                  </ImageBackground>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      );
    } else {
      return <ActivityIndicator />;
    }
  }
}

const styles = StyleSheet.create({
  images: {
    width: 200,
    height: 200,
  },
  container: {
      marginTop: 20
  },
  body: {
      backgroundColor: '#2b2d42',
  }
});
